﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using Tam.YRS2012.Refresha.Data;

namespace Tam.YRS2012.Refresha.Services
{
  public class StatisticsService
  {
    private ObjectContext Database { get; set; }
    private ObjectSet<Statistic> Data { get; set; }

    public StatisticsService(ModelContainer database)
    {
      Database = database;
      Data = database.Statistics;
    }

    public IEnumerable<Statistic> GetStatistics(byte metric, int year)
    {
      return Data.Where(e => e.eMetric == metric && e.Year == year);
    }

    public IEnumerable<int> GetYears(byte metric)
    {
      return Data
        .Where(e => e.eMetric == metric)
        .GroupBy(e => e.Year)
        .Select(e => e.Key);
    }
  }
}
