﻿using System;
using System.Collections.Generic;
using System.Web;
using Tam.YRS2012.Refresha.Data;

namespace Tam.YRS2012.Refresha.Services
{
  public class LEDService
  {
    public Dictionary<byte, float> LEDs
    {
      get
      {
        if (HttpContext.Current.Application["LEDs"] == null
          || !(HttpContext.Current.Application["LEDs"] is Dictionary<byte, float>))
        {
          HttpContext.Current.Application["LEDs"] = new Dictionary<byte, float>
          {
            { (byte)StatisticRegion.EastMidlands, 0 },
            { (byte)StatisticRegion.EastOfEngland, 0 },
            { (byte)StatisticRegion.GreaterLondon, 0 },
            { (byte)StatisticRegion.NorthEastEngland, 0 },
            { (byte)StatisticRegion.NorthWestEngland, 0 },
            { (byte)StatisticRegion.SouthEastEngland, 0 },
            { (byte)StatisticRegion.SouthWestEngland, 0 },
            { (byte)StatisticRegion.WestMidlands, 0 },
            { (byte)StatisticRegion.YorkshireAndTheHumber, 0 }
          };
        }

        return HttpContext.Current.Application["LEDs"] as Dictionary<byte, float>;
      }
      private set { HttpContext.Current.Application["LEDs"] = value; }
    }

    public byte? Active
    {
      get
      {
        if (HttpContext.Current.Application["Active"] != null
          && !(HttpContext.Current.Application["Active"] is byte?))
        {
          HttpContext.Current.Application["Active"] = null;
        }

        return HttpContext.Current.Application["Active"] as byte?;
      }
      set { HttpContext.Current.Application["Active"] = value; }
    }

    public void SetLED(StatisticRegion region, float percentage)
    {
      if (percentage < 0 || percentage > 1) throw new Exception("lolno");

      LEDs[(byte) region] = percentage;
    }
  }
}
