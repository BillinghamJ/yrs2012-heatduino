﻿using System.Collections.Generic;

namespace Tam.YRS2012.Refresha.Website.Models
{
  public class LEDPoll
  {
    public byte? Active { get; set; }
    public Dictionary<byte, float> LEDs { get; set; } 
  }
}
