﻿namespace Tam.YRS2012.Refresha.Website.Models
{
  public class Statistic
  {
    public static Statistic Map(Data.Statistic statistic, float min, float max)
    {
      // Damn automapper caching... -_-
      return new Statistic
      {
        Metric = statistic.eMetric,
        Region = statistic.eRegion,
        Value = statistic.Value,
        Year = statistic.Year,
        Percentage = Normalise(statistic.Value, min, max)
      };
    }

    public float Value { get; set; }
    public byte Metric { get; set; }
    public byte Region { get; set; }
    public int Year { get; set; }
    public float Percentage { get; set; }

    private static float Normalise(float num, float min, float max)
    {
      var range = (max - min);

      return ((num - min) / range);
    }
  }
}
