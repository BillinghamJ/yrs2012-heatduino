﻿using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tam.YRS2012.Refresha.Data;
using Tam.YRS2012.Refresha.Services;
using Tam.YRS2012.Refresha.Website.Models;

namespace Tam.YRS2012.Refresha.Website.Controllers
{
  public class ApiController : Controller
  {
    public StatisticsService StatisticsService { get; set; }
    public LEDService LEDService { get; set; }

    public ApiController(StatisticsService statisticsService, LEDService ledService)
    {
      StatisticsService = statisticsService;
      LEDService = ledService;
    }

    public JsonResult GetYears(byte metric)
    {
      var years = StatisticsService.GetYears(metric);

      if (years == null || !years.Any())
        return Json(false, JsonRequestBehavior.AllowGet);

      return Json(years, JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetStatistics(byte metric, int year)
    {
      var stats = StatisticsService.GetStatistics(metric, year);

      if (stats == null || !stats.Any())
        return Json(false, JsonRequestBehavior.AllowGet);
      
      var min = stats.Min(e => e.Value);
      var max = stats.Max(e => e.Value);

      var output = stats.Select(e => Models.Statistic.Map(e, min, max));

      foreach (var led in output)
      {
        LEDService.SetLED((StatisticRegion)led.Region, led.Percentage);
      }

      return Json(output, JsonRequestBehavior.AllowGet);
    }

    public ActionResult GetLEDs()
    {
      var json = JsonConvert.SerializeObject(new LEDPoll { LEDs = LEDService.LEDs, Active = LEDService.Active });

      return Content(json);
    }

    public void SetActiveRegion(byte? metric = null)
    {
      LEDService.Active = metric;
    }
  }
}
