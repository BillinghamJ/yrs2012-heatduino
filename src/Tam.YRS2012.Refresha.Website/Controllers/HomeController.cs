﻿using System.Web.Mvc;

namespace Tam.YRS2012.Refresha.Website.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      return View();
    }
  }
}
