﻿using System;
using System.Web;

namespace Tam.YRS2012.Refresha.Website
{
  public class HeatmapProxy : IHttpHandler
  {
    public void ProcessRequest(HttpContext context)
    {
      string k = context.Request["k"];
      string u = context.Request["u"];
      string lat1 = context.Request["lat1"];
      string lat2 = context.Request["lat2"];
      string lon1 = context.Request["lon1"];
      string lon2 = context.Request["lon2"];
      string b = context.Request["b"];
      string w = context.Request["w"];
      string h = context.Request["h"];
      string d = context.Request["d"];

      string x = context.Request["x"];

      hmWS.HeatmapGenerate2WSSoapClient hm = new hmWS.HeatmapGenerate2WSSoapClient();
      string path = hm.GetImagePathDecayColor(k, u, Convert.ToDouble(lat1), Convert.ToDouble(lat2), Convert.ToDouble(lon1), Convert.ToDouble(lon2), w, h, d, b, x, "1");

      context.Response.Write(path); // Just write out the path we got, the javascript api knows what to do.
    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}
