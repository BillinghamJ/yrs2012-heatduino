﻿var myHeatmap = new GEOHeatmap();
var myData = null;
var map = null;
var google = google || {};
var metric = 0;
var year = 0;

$(document).ready(function () {
    $('#map_canvas').height(window.innerHeight - 60);

    var myOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

    getLocation();

    if (window.location.hash.length > 1) {
        var args = window.location.hash.substr(1).split(':');

        metric = Number(args[0]) || 0;
        year = Number(args[1]) || 0;
    }

    $('#metrics select').val(metric);

    $.get('/api/GetYears/', { metric: metric }, function (data) {
        if (year == 0)
            year = data[0];

        var options = '';

        $.each(data, function () {
            options += '<option value="' + this + '">' + this + '</option>';
        });

        $('#years option').html(options);
        $('#years select').val(year);

        $('#metrics select').change(function () {
            window.location.hash = $('#metrics select').val();
            window.location.reload();
        });

        $('#years select').change(function () {
            window.location.hash = $('#metrics select').val() + ':' + $('#years select').val();
            window.location.reload();
        });

        loadHeatmap(metric, year);
    });
});

function loadHeatmap(metric, year) {
    $.getJSON('/api/GetStatistics/', { metric: metric, year: year }, function (data) {
        myData = new Array();

        $.each(data, function (i) {
            var regLatLon = getLatLon(data[i].Region);
            myData.push(regLatLon[0], regLatLon[1], data[i].Value);

            var initialLocation = new google.maps.LatLng(regLatLon[0], regLatLon[1]);

            var marker = new google.maps.Marker({
                position: initialLocation,
                animation: google.maps.Animation.DROP
            });

            google.maps.event.addListener(marker, 'click', function () {
                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent('<h3>'+metricToString(metric)+" "+data[i].Year+':</h3><p>' + addCommas(data[i].Value) + '</p>');
                infoWindow.open(map, this);
            });

            marker.setMap(map);
        }); 

        myHeatmap.Init(1440, 739);
        myHeatmap.SetBoost(250);
        myHeatmap.SetDecay(1);
        myHeatmap.SetData(myData);
        myHeatmap.SetProxyURL('/HeatmapProxy.ashx');

        google.maps.event.addListener(map, 'idle', function () {
            myHeatmap.AddOverlay(this, myHeatmap);
        });
    });
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(initialLocation);

            var marker = new google.maps.Marker({
                position: initialLocation,
                animation: google.maps.Animation.DROP
            });

            
        }, handleNoGeolocation);
    }
    else { handleNoGeolocation(); }
}

function handleNoGeolocation() {
    map.setCenter(new google.maps.LatLng(0, 0));
}

function getLatLon(regionId) {
    switch (regionId) {
        case 0: return [52.7, -0.5];
        case 1: return [52.2, -0.4];
        case 2: return [51.5, -0.1];
        case 3: return [54.9, -1.8];
        case 4: return [54.0, -2.8];
        case 5: return [51.2, -0.6];
        case 6: return [51.0, -3.1];
        case 7: return [52.5, -2.1];
        case 8: return [54.0, -1.1];
        
        default: return [0, 0];
    }
}

function addCommas(nStr) {
    nStr += '';
    
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    
    var rgx = /(\d+)(\d{3})/;
    
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    
    return x1 + x2;
}

function metricToString(metricNumber) {
    switch (metricNumber) {
        case 0: return 'Instances of crime';
        case 1: return 'Gross income per capita';
        case 2: return 'NHS spending';

        default: return '';
    }
}
