﻿namespace Tam.YRS2012.Refresha.Data
{
  public partial class Statistic
  {
    public StatisticRegion Region
    {
      get { return (StatisticRegion)eRegion; }
      set { eRegion = (byte)value; }
    }

    public StatisticMetric Metric
    {
      get { return (StatisticMetric)eMetric; }
      set { eMetric = (byte)value; }
    }
  }

  public enum StatisticRegion : byte
  {
    EastMidlands = 0,
    EastOfEngland = 1,
    GreaterLondon = 2,
    NorthEastEngland = 3,
    NorthWestEngland = 4,
    SouthEastEngland = 5,
    SouthWestEngland = 6,
    WestMidlands = 7,
    YorkshireAndTheHumber = 8
  }

  public enum StatisticMetric : byte
  {
    CrimeRate = 0,
    Income = 1,
    NHSSpending = 2
  }
}
